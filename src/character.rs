use crate::classes::{self, Class};
use crate::spell::Spell;

#[derive(Clone)]
pub struct Character {
    pub health: i32,
    max_health: i32,
    pub mana: i32,
    max_mana: i32,
    level: i32,
    class: classes::Class,
    strength: i32,
    dexterity: i32,
    intelligence: i32,
    spells: Vec<Spell>,
    spells_learned: i32,
    //equipment: equipment,
    //inventory: inventory,
}

impl Character {
    const BASE_MANA:i32 = 100;
    const BASE_HEALTH:i32 = 100;

    fn calculate_mana(intelligence: i32) -> i32 {
        return Character::BASE_MANA + intelligence* 10;
    }

    fn calculate_health(strength: i32) -> i32 {
        return Character::BASE_HEALTH + strength * 10;
    }

    pub fn new(class: Class) -> Character {
        let str = class.clone().strength;
        let dex = class.clone().dexterity;
        let int = class.clone().intelligence;

        Character {
            health: Character::calculate_health(str),
            max_health: Character::calculate_health(str),
            mana: Character::calculate_mana(int),
            max_mana: Character::calculate_mana(int),
            level: 1,
            class: class.clone(),
            strength: str,
            dexterity: dex,
            intelligence: int,
            spells: Vec::new(),
            spells_learned: 0,
        }
    }

    // Getter and setter methods

    pub fn add_strength(&mut self, amount: i32) {
        self.strength += amount;
        self.max_health = Character::calculate_health(self.strength);
    }

    pub fn get_strength(&self) -> i32 {
        return self.strength;
    }

    pub fn add_dexterity(&mut self, amount: i32) {
        self.dexterity += amount;
    }

    pub fn get_dexterity(&self) -> i32 {
        return self.dexterity;
    }

    pub fn add_intelligence(&mut self, amount: i32) {
        self.intelligence += amount;
        self.max_mana = Character::calculate_mana(self.intelligence);
    }

    pub fn get_intelligence(&self) -> i32 {
        return self.intelligence;
    }

    pub fn add_level(&mut self, amount: i32) {
        self.level += amount;
        self.add_strength(1);
        self.add_intelligence(1);
        self.add_dexterity(1);
    }

    pub fn get_level(&self) -> i32 {
        return self.level;
    }

    pub fn get_class(&self) -> Class {
        return self.class.clone();
    }

    pub fn add_spell(&mut self, spell: Spell) {
        self.spells_learned += 1;
        self.spells.push(spell);
    }

    pub fn get_spells(&self) -> Vec<Spell> {
        return self.spells.clone();
    }

    pub fn get_spells_learned(&self) -> i32 {
        return self.spells_learned;
    }

    pub fn get_max_health(&self) -> i32 {
        return self.max_health;
    }

    pub fn get_max_mana(&self) -> i32 {
        return self.max_mana;
    }

    // Character methods

    pub fn cast(&mut self, mut spell: Spell, mut target: Character) -> bool {
        if self.level >= spell.level && self.mana >= spell.cost && spell.cast() {
            self.mana -= spell.cost;
            target.health -= spell.damage;
        }
        return false;
    }
}
