#[derive(Clone, Default)]
pub struct Spell {
    pub name: String,
    pub cost: i32,
    pub damage: i32,
    uses: i32,
    pub max_uses: i32,
    pub level: i32,
    pub description: String,
}

impl Spell {
    pub fn new() -> Spell {
        Spell {
            name: String::new(),
            cost: 0,
            damage: 0,
            uses: 0,
            max_uses: 0,
            level: 0,
            description: String::new(),
        }
    }

    pub fn new_w_args(
        name: String,
        cost: i32,
        damage: i32,
        max_uses: i32,
        level: i32,
        description: String,
    ) -> Spell {
        Spell {
            name,
            cost,
            damage,
            uses: 0,
            max_uses,
            level,
            description,
        }
    }

    pub fn get_uses(&self) -> i32 {
        return self.uses;
    }

    pub fn cast(&mut self) -> bool {
        if self.max_uses > 0 {
            if self.uses > 0 {
                self.uses -= 1;
                return true;
            }
        } else {
            return true;
        }
        return false;
    }
}
