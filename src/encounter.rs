use crate::character::Character;
use rand::Rng;

pub trait Encounter {
    fn title(&self) -> &str;
    fn start(&self, player: &mut Character);
}

impl Eq for dyn Encounter {}

impl PartialEq for dyn Encounter {
    fn eq(&self, other: &Self) -> bool {
        self.title() == other.title()
    }
}

impl std::hash::Hash for dyn Encounter {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.title().hash(state);
    }
}

impl Clone for Box<dyn Encounter> {
    fn clone(&self) -> Box<dyn Encounter> {
        match self.title() {
            "Nothing" => Box::new(Nothing),
            "Bandit" => Box::new(Bandit),
            "Goblin" => Box::new(Goblin),
            "Bear" => Box::new(Bear),
            "Merchant" => Box::new(Merchant),
            "Innkeeper" => Box::new(Innkeeper),
            "Guard" => Box::new(Guard),
            "Knight" => Box::new(Knight),
            "King" => Box::new(King),
            _ => Box::new(Nothing),
        }
    }
}

#[derive(Clone)]
pub struct Nothing;
#[derive(Clone)]
pub struct Bandit;
#[derive(Clone)]
pub struct Goblin;
#[derive(Clone)]
pub struct Bear;
#[derive(Clone)]
pub struct Merchant;
#[derive(Clone)]
pub struct Innkeeper;
#[derive(Clone)]
pub struct Guard;
#[derive(Clone)]
pub struct Knight;
#[derive(Clone)]
pub struct King;

pub trait Fight {
    fn fight(&self, player: &mut Character);
}

//create Encounters
impl Encounter for Nothing {
    fn title(&self) -> &str {
        "Nothing"
    }
    fn start(&self, player: &mut Character) {
        println!("You encounter nothing, you can go to the next zone.");
    }
}

impl Fight for Bandit{
    fn fight(&self, player: &mut Character) {
        // fight logic
        let mut bandit_health = 10;
        let bandit_damage = 2;
        let mut rng = rand::thread_rng();
        while player.health > 0 && bandit_health > 0 {
            let player_attack = rng.gen_range(player.get_strength()/2..player.get_strength());
            bandit_health -= player_attack;
            println!("You hit the bandit for {} damage", player_attack);
            if bandit_health > 0 {
                let bandit_attack = rng.gen_range(bandit_damage/2..bandit_damage);
                player.health -= bandit_attack;
                println!("The bandit hits you for {} damage", bandit_attack);
            }
        }
        if player.health > 0 {
            println!("You defeated the bandit!");
        } else {
            println!("You were defeated by the bandit...");
        }
    }
}

impl Encounter for Bandit {
    fn title(&self) -> &str {
        "Bandit"
    }
    fn start(&self, player: &mut Character) {
        println!("You encounter a bandit");
        // asks the user if they want to fight or run
        // if they fight, they will fight the bandit
        // if they run, they will have a chance to run
        // if they fail to run, they will fight the bandit
        let mut novalid = true;
        while novalid {
            println!("1. Fight",);
            println!("2. Try to run (dexterity check)");
            let mut choice = String::new();
            std::io::stdin()
                .read_line(&mut choice)
                .expect("Failed to read line");
            let choice: i32 = choice.trim().parse().expect("Please type a number!");
            if choice < 1 || choice > 4 {
                println!("Please select a valid option!");
            } else {
                match choice {
                    1 => {
                        novalid = false;
                        println!("You fight!");
                        self.fight(player);
                    }
                    2 => {
                        novalid = false;
                        println!("Dex check.");
                        // dex check logic
                        // launch a d20
                        let roll = rand::thread_rng().gen_range(1..=20);
                        // if dex of player is higher than the roll, they run away
                        let dex_check = player.get_dexterity() > roll;
                        if dex_check {
                            println!("You successfully run away from the bandit.");
                        } else {
                            println!("You failed to run away. Prepare for a fight!");
                            self.fight(player);
                        }
                    }
                    _ => {
                        println!("Please select a valid option!");
                    }
                }
            }
        }
    }
}

impl Encounter for Goblin {
    fn title(&self) -> &str {
        "Goblin"
    }
    fn start(&self, player: &mut Character) {
        println!("You encounter a goblin");
    }
}

impl Encounter for Bear {
    fn title(&self) -> &str {
        "Bear"
    }
    fn start(&self, player: &mut Character) {
        println!("You encounter a bear");
    }
}

impl Encounter for Merchant {
    fn title(&self) -> &str {
        "Merchant"
    }
    fn start(&self, player: &mut Character) {
        println!("You encounter a merchant");
    }
}

impl Encounter for Innkeeper {
    fn title(&self) -> &str {
        "Innkeeper"
    }
    fn start(&self, player: &mut Character) {
        println!("You encounter an innkeeper");
    }
}

impl Encounter for Guard {
    fn title(&self) -> &str {
        "Guard"
    }
    fn start(&self, player: &mut Character) {
        println!("You encounter a guard");
    }
}

impl Encounter for Knight {
    fn title(&self) -> &str {
        "Knight"
    }
    fn start(&self, player: &mut Character) {
        println!("You encounter a knight");
    }
}

impl Encounter for King {
    fn title(&self) -> &str {
        "King"
    }
    fn start(&self, player: &mut Character) {
        println!("You encounter a king");
    }
}
