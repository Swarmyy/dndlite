use crate::character::Character;
use crate::encounter::Encounter;
use crate::zones::Zones;
use rand::Rng;
use std::collections::HashMap;

pub struct Zone {
    name: String,
    message: String,
    end_message: String,
    encounters: HashMap<Box<dyn Encounter>, f64>,
    next_zones: HashMap<Zones, f64>,
}

impl Zone {
    pub fn new(name: String, display_message: String, end_message: String) -> Zone {
        Zone {
            name,
            message: display_message,
            end_message,
            encounters: HashMap::new(),
            next_zones: HashMap::new(),
        }
    }

    pub fn get_name(&self) -> &str {
        &self.name
    }

    pub fn display_message(&self) -> &str {
        &self.message
    }

    pub fn display_end_message(&self) -> &str {
        &self.end_message
    }

    pub fn add_encounter(&mut self, encounter: Box<dyn Encounter>, chance: f64) {
        if chance < 0.0 || chance > 1.0 {
            panic!("Chance must be between 0 and 1");
        }
        // If the encounter already exists, add the chance to the existing chance
        if self.encounters.contains_key(&encounter) {
            let current_chance = self.encounters.get(&encounter).unwrap();
            self.encounters.insert(encounter, current_chance + chance);
            return;
        }
        // panic if sum of chances in encounters is greater than 1
        let mut total = 0.0;
        for chance in self.encounters.values() {
            total += chance;
        }
        if total + chance > 1.0 {
            panic!("Total chance of encounters cannot exceed 1");
        }
        // add encounter
        self.encounters.insert(encounter, chance);
    }

    // returns an encounter randomly picked
    pub fn get_encounter(&self, player: &mut Character) -> Box<dyn Encounter> {
        let mut rng = rand::thread_rng();
        let mut total = 0.0;
        for chance in self.encounters.values() {
            total += chance;
        }
        let mut roll = rng.gen_range(0.0..total);
        for (encounter, chance) in self.encounters.iter() {
            if roll < *chance {
                return (*encounter).clone();
            }
            roll -= chance;
        }
        panic!("No encounter found");
    }

    pub fn del_encounter(&mut self, encounter: Box<dyn Encounter>) {
        self.encounters.remove(&encounter);
    }

    pub fn add_template_zone(&mut self, template_zone: Zones, chance: f64) {
        if chance < 0.0 || chance > 1.0 {
            panic!("Chance must be between 0 and 1");
        }
        // If the zone already exists, add the chance to the existing chance
        if self.next_zones.contains_key(&template_zone) {
            let current_chance = self.next_zones.get(&template_zone).unwrap();
            self.next_zones
                .insert(template_zone, current_chance + chance);
            return;
        }
        // panic if sum of chances in next_zones is greater than 1
        let mut total = 0.0;
        for chance in self.next_zones.values() {
            total += chance;
        }
        if total + chance > 1.0 {
            panic!("Total chance of next zones cannot exceed 1");
        }
        // add zone
        self.next_zones.insert(template_zone, chance);
    }

    // returns a zone randomly picked
    pub fn get_next_template_zone(&self) -> Zones {
        let mut rng = rand::thread_rng();
        let mut total = 0.0;
        for chance in self.next_zones.values() {
            total += chance;
        }
        let mut roll = rng.gen_range(0.0..total);
        for (tz, chance) in self.next_zones.iter() {
            if roll < *chance {
                return tz.clone();
            }
            roll -= chance;
        }
        panic!("No next zone found");
    }

    pub fn del_template_zone(&mut self, template_zone: Zones) {
        self.next_zones.remove(&template_zone);
    }
}

impl Eq for Zone {}

impl PartialEq for Zone {
    fn eq(&self, other: &Self) -> bool {
        self.get_name() == other.get_name()
    }
}

impl std::hash::Hash for Zone {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.get_name().hash(state);
    }
}

impl Clone for Zone {
    fn clone(&self) -> Zone {
        Zone {
            name: self.name.clone(),
            message: self.message.clone(),
            end_message: self.end_message.clone(),
            encounters: self.encounters.clone(),
            next_zones: self.next_zones.clone(),
        }
    }
}
