mod character;
mod classes;
mod encounter;
mod spell;
mod zone;
mod zones;

use character::Character;
use classes::{Class, Classes};
use encounter::Encounter;
use spell::Spell;
use zone::Zone;
use zones::Zones;

fn class_choice_text() -> Class {
    println!("Please select a class:");
    // print out the classes
    let classes = [Classes::Warrior, Classes::Mage, Classes::Rogue];
    for (i, class) in classes.iter().enumerate() {
        println!("{}. {}", i + 1, class.value().name);
    }
    // ask for a number between the size of the enum and retries as long as it fails
    let mut class = String::new();
    std::io::stdin()
        .read_line(&mut class)
        .expect("Failed to read line");
    let class: i32 = class.trim().parse().expect("Please type a number!");
    if class < 1 || class > classes.len() as i32 {
        println!("Please select a valid class!");
        return class_choice_text();
    }
    // return the class as Class type
    return classes[class as usize - 1].value();
}

fn spell_choice_text(user: Character) -> Spell {
    println!("Please select a spell:");
    // print out the spells for the classes of the user
    for (i, spell) in user.get_class().spells.iter().enumerate() {
        println!("{}. {}", i + 1, spell.name);
    }
    // ask for a number between the size of the enum and retries as long as it fails
    let mut spell = String::new();
    std::io::stdin()
        .read_line(&mut spell)
        .expect("Failed to read line");
    let spell: i32 = spell.trim().parse().expect("Please type a number!");
    if spell < 1 || spell > user.get_class().spells.len() as i32 {
        println!("Please select a valid spell!");
        return spell_choice_text(user);
    }
    // return the spell as Spell type
    return user.get_class().spells[spell as usize - 1].clone();
}

// print out the character and ask for a spell to learn if the user has leveled up
fn print_character(mut user: Character) -> Character {
    println!(
        "You are a level {} {}!",
        user.get_level(),
        user.get_class().name
    );
    println!("Strength: {}", user.get_strength());
    println!("Dexterity: {}", user.get_dexterity());
    println!("Intelligence: {}", user.get_intelligence());
    println!("Health: {}/{}", user.health, user.get_max_health());
    println!("Mana: {}/{}", user.mana, user.get_max_mana());
    println!("Spells:");
    for spell in user.get_spells() {
        println!("{} - {}", spell.name, spell.description);
    }
    // spell learning logic
    if user.get_level() < user.get_spells_learned() {
        println!("You have a new spell to learn!");
        println!("Please select a spell to learn:");
        for (i, spell) in user.get_class().spells.iter().enumerate() {
            println!("{}. {}", i + 1, spell.name);
        }
        let spell = spell_choice_text(user.clone());
        user.add_spell(spell.clone());
    }
    return user;
}

fn get_encounter(zone: Zone, mut user: Character) -> Box<dyn Encounter> {
    zone.get_encounter(&mut user)
}

fn start_adventure(mut user: Character, template_zone: Zones, mut dealt: bool) {
    let mut zone = template_zone.value();
    template_zone.add_encounters(&mut zone);
    template_zone.add_next_zones(&mut zone);
    println!("{}", zone.display_message());
    // encounter with I/O interactions
    if !dealt {
        let encounter = get_encounter(zone.clone(), user.clone());
        // add logic to all encounters
        encounter.start(&mut user);
    }
    dealt = true;
    zone.display_end_message();
    let next_tz = zone.get_next_template_zone();
    println!("What would you like to do?");
    println!("1. Go to next zone ({})", next_tz.value().get_name());
    println!("3. View Character");
    println!("4. Quit");
    let mut choice = String::new();
    std::io::stdin()
        .read_line(&mut choice)
        .expect("Failed to read line");
    let choice: i32 = choice.trim().parse().expect("Please type a number!");
    if choice < 1 || choice > 4 {
        println!("Please select a valid option!");
        return start_adventure(user, template_zone, dealt);
    }
    match choice {
        1 => {
            println!("You have chosen to go to the next zone!");
            start_adventure(user,next_tz, false)
        }
        2 => {
            println!("Please select a valid option!");
            return start_adventure(user, template_zone, dealt);
        }
        3 => {
            user = print_character(user);
            return start_adventure(user, template_zone, dealt);
        }
        4 => {
            println!("You have quit the game!");
            return;
        }
        _ => {
            println!("Please select a valid option!");
            return start_adventure(user, template_zone, dealt);
        }
    }
}

fn main() {
    // Welcome message
    println!("Welcome to D&D Like Lite");
    // Get class
    let class = class_choice_text();
    // Set class
    let mut user = Character::new(class);
    user = print_character(user);
    // Start adventure
    start_adventure(user, Zones::Road, false);
}
