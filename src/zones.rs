use crate::encounter::{Bandit, Bear, Goblin, Guard, Innkeeper, King, Knight, Merchant, Nothing};
use crate::zone::Zone;

// A list of Zones : road, town, forest, mountain, cave, castle
#[derive(Eq, Hash, PartialEq, Clone)]
pub enum Zones {
    Road,
    Town,
    Forest,
    Mountain,
    Cave,
    Castle,
}

impl Zones {
    pub fn value(&self) -> Zone {
        match self {
            Zones::Road => Zone::new(
                "Road".to_string(),
                "You are on a road.".to_string(),
                "You can go to the town or the forest.".to_string(),
            ),
            Zones::Town => Zone::new(
                "Town".to_string(),
                "You are in a town.".to_string(),
                "You can go to the road, the forest, the mountain or the castle.".to_string(),
            ),
            Zones::Forest => Zone::new(
                "Forest".to_string(),
                "You are in a forest.".to_string(),
                "You can go to the road or the town.".to_string(),
            ),
            Zones::Mountain => Zone::new(
                "Mountain".to_string(),
                "You are in a mountain.".to_string(),
                "You can go to the town or the cave.".to_string(),
            ),
            Zones::Cave => Zone::new(
                "Cave".to_string(),
                "You are in a cave.".to_string(),
                "You can go to the forest, the mountain or the castle.".to_string(),
            ),
            Zones::Castle => Zone::new(
                "Castle".to_string(),
                "You are in a castle.".to_string(),
                "You can go to the town or the cave.".to_string(),
            ),
        }
    }

    // add encounters to the zone
    pub fn add_encounters(&self, zone: &mut Zone) {
        match self {
            Zones::Road => {
                zone.add_encounter(Box::new(Nothing), 0.5);
                zone.add_encounter(Box::new(Bandit), 0.3);
                zone.add_encounter(Box::new(Goblin), 0.2);
            }
            Zones::Town => {
                zone.add_encounter(Box::new(Merchant), 0.3);
                zone.add_encounter(Box::new(Innkeeper), 0.5);
                zone.add_encounter(Box::new(Guard), 0.2)
            }
            Zones::Forest => {
                zone.add_encounter(Box::new(Nothing), 0.3);
                zone.add_encounter(Box::new(Bear), 0.3);
                zone.add_encounter(Box::new(Goblin), 0.4);
            }
            Zones::Mountain => {
                zone.add_encounter(Box::new(Nothing), 0.2);
                zone.add_encounter(Box::new(Knight), 0.4);
                zone.add_encounter(Box::new(Guard), 0.4);
            }
            Zones::Cave => {
                zone.add_encounter(Box::new(Nothing), 0.2);
                zone.add_encounter(Box::new(Bear), 0.5);
                zone.add_encounter(Box::new(Goblin), 0.3);
            }
            Zones::Castle => {
                zone.add_encounter(Box::new(King), 0.1);
                zone.add_encounter(Box::new(Knight), 0.4);
                zone.add_encounter(Box::new(Guard), 0.5);
            }
        }
    }

    // add next zones to the zone
    pub fn add_next_zones(&self, zone: &mut Zone) {
        match self {
            Zones::Road => {
                zone.add_template_zone(Zones::Town, 0.5);
                zone.add_template_zone(Zones::Forest, 0.5);
            }
            Zones::Town => {
                zone.add_template_zone(Zones::Road, 0.25);
                zone.add_template_zone(Zones::Forest, 0.25);
                zone.add_template_zone(Zones::Mountain, 0.25);
                zone.add_template_zone(Zones::Castle, 0.25);
            }
            Zones::Forest => {
                zone.add_template_zone(Zones::Road, 0.33);
                zone.add_template_zone(Zones::Town, 0.33);
                zone.add_template_zone(Zones::Cave, 0.33);
            }
            Zones::Mountain => {
                zone.add_template_zone(Zones::Town, 0.5);
                zone.add_template_zone(Zones::Cave, 0.5);
            }
            Zones::Cave => {
                zone.add_template_zone(Zones::Forest, 0.33);
                zone.add_template_zone(Zones::Mountain, 0.33);
                zone.add_template_zone(Zones::Castle, 0.33);
            }
            Zones::Castle => {
                zone.add_template_zone(Zones::Town, 0.5);
                zone.add_template_zone(Zones::Cave, 0.5);
            }
        }
    }
}
