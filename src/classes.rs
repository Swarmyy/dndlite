use crate::spell::Spell;

#[derive(Clone)]
pub struct Class {
    pub name: String,
    pub strength: i32,
    pub dexterity: i32,
    pub intelligence: i32,
    pub spells: Vec<Spell>,
}

impl Class {
    pub fn new() -> Class {
        Class {
            name: String::new(),
            strength: 0,
            dexterity: 0,
            intelligence: 0,
            spells: Vec::new(),
        }
    }
}

pub enum Classes {
    Warrior,
    Mage,
    Rogue,
}

impl Classes {
    pub fn value(&self) -> Class {
        match *self {
            Classes::Warrior => Class {
                name: String::from("Warrior"),
                strength: 10,
                dexterity: 5,
                intelligence: 5,
                spells: [
                    Spell::new_w_args(
                        "Slash".to_string(),
                        10,
                        10,
                        0,
                        1,
                        "A powerful slash attack".to_string(),
                    ),
                    Spell::new_w_args(
                        "Bash".to_string(),
                        10,
                        10,
                        0,
                        2,
                        "A powerful bash attack".to_string(),
                    ),
                    Spell::new_w_args(
                        "Stab".to_string(),
                        10,
                        10,
                        0,
                        3,
                        "A powerful stab attack".to_string(),
                    ),
                ]
                .to_vec(),
            },
            Classes::Mage => Class {
                name: String::from("Mage"),
                strength: 5,
                dexterity: 5,
                intelligence: 10,
                spells: [
                    Spell::new_w_args(
                        "Fireball".to_string(),
                        10,
                        10,
                        0,
                        1,
                        "A powerful fireball attack".to_string(),
                    ),
                    Spell::new_w_args(
                        "Iceball".to_string(),
                        20,
                        20,
                        0,
                        2,
                        "A powerful iceball attack".to_string(),
                    ),
                    Spell::new_w_args(
                        "Lightning".to_string(),
                        30,
                        30,
                        0,
                        3,
                        "A powerful lightning attack".to_string(),
                    ),
                ]
                .to_vec(),
            },
            Classes::Rogue => Class {
                name: String::from("Rogue"),
                strength: 5,
                dexterity: 10,
                intelligence: 5,
                spells: [
                    Spell::new_w_args(
                        "Backstab".to_string(),
                        10,
                        10,
                        0,
                        1,
                        "A powerful backstab attack".to_string(),
                    ),
                    Spell::new_w_args(
                        "Poison".to_string(),
                        0,
                        30,
                        10,
                        2,
                        "A powerful poison attack".to_string(),
                    ),
                    Spell::new_w_args(
                        "Sneak".to_string(),
                        40,
                        40,
                        0,
                        3,
                        "A powerful sneak attack".to_string(),
                    ),
                ]
                .to_vec(),
            },
        }
    }
}
